package com.zpark.hbase.mr.domo;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;

public class SumMoneyJob {

    public static void main(String[] args) throws Exception{
        Configuration config = HBaseConfiguration.create() ;
        //Job job = new Job(config,"sumMyMoney") ;
        Job job = Job.getInstance(config,"sumMyMoney") ;
        job.setJarByClass(SumMoneyJob.class);

        Scan scan  = new Scan() ;
        //Scan在返回数据到客户之前，在服务端缓存的数据行数，默认值为100
        scan.setCaching(500) ;
        //在MR中这里要设成false
        scan.setCacheBlocks(false) ;

        TableMapReduceUtil.initTableMapperJob("mymoney",scan,MyMapper.class, Text.class, IntWritable.class,job);

        TableMapReduceUtil.initTableReducerJob("mymoney", MyReduce.class,job);

        job.setNumReduceTasks(1);
        boolean result = job.waitForCompletion(true) ;
        if(!result){
            System.out.println("出错");
        }



    }
}
