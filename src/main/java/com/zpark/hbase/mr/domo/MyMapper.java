package com.zpark.hbase.mr.domo;

import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class MyMapper  extends TableMapper<Text, IntWritable> {
    private Text text = new Text("allIncomes") ;

    public void map(ImmutableBytesWritable row, Result result, Context context) throws IOException, InterruptedException {
        //获取收入的值
        IntWritable income = new IntWritable(Bytes.toInt(result.getValue("info".getBytes(),"income".getBytes()))) ;
        context.write(text,income);
    }
}
