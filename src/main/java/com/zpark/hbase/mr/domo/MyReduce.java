package com.zpark.hbase.mr.domo;

import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.Iterator;

public class MyReduce extends TableReducer<Text, IntWritable, ImmutableBytesWritable> {

    public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {

            int i = 0 ;
            for(IntWritable val :values){
                i += val.get() ;
            }

            Put put = new Put(Bytes.toBytes("total")) ;
            put.addColumn("info".getBytes(),"totoalIncome".getBytes(),Bytes.toBytes(i)) ;

            context.write(null,put);
    }
}
